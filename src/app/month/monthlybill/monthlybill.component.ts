import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

import { DateModel } from 'ng2-datepicker';

@Component({
  selector: 'monthlybill',
  templateUrl: './monthlybill.component.html',
  styleUrls: ['./monthlybill.component.css']
})
export class MonthlybillComponent{
  @Input()
  month;

  @Input()
  username;

  @Output()
  saveMonth = new EventEmitter<any>();

  newBill;
  addingNewBill:boolean = false;
  billNameControl = new FormControl('', Validators.compose([Validators.required]));
  billAmtControl = new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[0-9]{1,3}.[0-9]{2}$')
  ]));
  billPaidDateControl = new FormControl('', Validators.compose([Validators.required]));
  billPaidByControl = new FormControl('', Validators.compose([Validators.required]));
  utilityBillsForm = new FormGroup({
      name: this.billNameControl,
      amount: this.billAmtControl,
      paidBy: this.billPaidByControl,
      paidDate: this.billPaidDateControl
    });

  newExpense;
  addingNewExpense:boolean = false;
  noteControl = new FormControl('', Validators.compose([Validators.required]));
  amountControl = new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[0-9]{1,3}.[0-9]{2}$')
  ]));
  expensePaidByControl = new FormControl('', Validators.compose([Validators.required]));
  expensePaidDateControl = new FormControl('', Validators.compose([Validators.required]));
  newExpenseForm = new FormGroup({
      note: this.noteControl,
      amount: this.amountControl,
      paidBy: this.expensePaidByControl,
      expensePaidDate: this.expensePaidDateControl
    });

  today: string;
  billDayOfMonth: number;

  billDate: DateModel;
  expenseDate: DateModel;

  date = new Date();
  datePickerOptions={
    autoApply: true,
    firstWeekdaySunday: true,
    format: 'MM/DD/YYYY',
    style: 'normal'
  };

  constructor(private firebase: AngularFire) {
    let d = new Date(Date.now());
    this.today = (d.getMonth()+1) + '/' + d.getDate();
    this.billDayOfMonth = d.getDate();
  }

  hideYear() {
    if(document.getElementsByClassName("datepicker-calendar-top")[0]) {
      try{
        document.getElementsByClassName("datepicker-calendar-top")[0].getElementsByTagName("button")[0].innerHTML = '';
        document.getElementsByClassName("datepicker-calendar-top")[0].getElementsByTagName("button")[0].hidden = true;
        document.getElementsByClassName("datepicker-calendar-top")[0].getElementsByTagName("button")[0].disabled = true;
      } catch (e) {
        console.error(e);
      }
    }
  }

  createNewBill() {
    this.addingNewBill = true;
    this.newBill = {
      name: "",
      amount: '',
      paidBy: ''
    }
  }
  createNewExpense() {
    this.addingNewExpense = true;
    this.newExpense = {
      note: "",
      amount: '',
      paidBy: ''
    }
  }

  doSaveExpense(){
    console.log("doSaveMonth " + JSON.stringify(this.month));
    this.newExpense.dateRegistered = this.expenseDate.momentObj.format('MM/DD/YYYY');
    if(this.month.others){
      this.month.others = [...this.month.others, this.newExpense];
    } else {
      this.month.others = [this.newExpense];
    }
    this.saveMonth.emit(this.month);
    this.clearFields();
  }

  doSaveBill(){
    console.log("doSaveBill " + JSON.stringify(this.month));
    this.newBill.dateRegistered = this.billDate.momentObj.format('MM/DD/YYYY');
    if(this.month.bills){
      this.month.bills.unshift(this.newBill);
    } else {
      this.month.bills = [this.newBill]
    }
    this.saveMonth.emit(this.month);
    this.clearFields();
  }

  verifyPaidByUs(userValue: string) {
    return userValue.toLowerCase()==='fvc' || userValue.toLowerCase()==='ckm';
  }

  clearFields(){
    this.utilityBillsForm.reset();
    this.addingNewBill = false;

    this.newExpenseForm.reset();
    this.addingNewExpense = false;
  }

  isValidAmount(amount){
    return Number(amount)>0 && Number(amount)<=999.99;
  }
}
