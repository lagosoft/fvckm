export interface IMonthlyBill {
  id:number,
  month: string,
  bills: [
    {
      name: string,
      amount: number,
      paidBy: string,
      dateRegistered: string
    }
  ],
  others: [
    {
      note: string,
      amount: number,
      paidBy: string,
      dateRegistered: string
    }
  ]
}
