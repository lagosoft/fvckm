require 'pg'
require 'sinatra'
require 'sinatra/cross_origin'
require 'json'
require 'date'

conn = PG::Connection.open("localhost",5432,"","","postgres","administrator","password")

configure do
  #Enable cross origin
  enable :cross_origin

  #INITIALIZE
  # running for 1st time then we populate the db with the 12 months for the year.
  res = conn.exec("SELECT COUNT(*) FROM fvckmoney.monthly_expenses")
  count = Integer(res[0]['count'])

  if count!=12
    12.times do |i|
      monthNumber = i+1
      monthName = Date::MONTHNAMES[monthNumber].to_s
      begin
        conn.exec("INSERT INTO fvckmoney.monthly_expenses (id, month, monthly_expense) VALUES (" + monthNumber.to_s + ", '" + monthName  + "', to_json('{\"month\":\"" +  monthName + "\"}'::json))")
        print "inserted " + monthName + "(" + monthNumber.to_s + ")\n"
      rescue
        print "skipped " + monthName + "\n"
      end
    end
  end
end

options '/*' do
  print "OPTIONS"
  # headers  "Access-Control-Allow-Headers" => "origin, x-requested-with, content-type"
  # status 200
  headers "Allow" => "HEAD,GET,PUT,DELETE,OPTIONS"

  # Needed for AngularJS
  headers "Access-Control-Allow-Headers" => "X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept"

  status 200
end

get '/months' do
  content_type :json
  print "MONTHS"

  res = conn.exec("SELECT id, monthly_expense::json FROM fvckmoney.monthly_expenses ORDER BY id")
  i=0
  dbJsonResult = ""
  res.each{
  	dbJsonResult.concat(res[i]['monthly_expense'].to_s)
  	i=i+1
    if i<12
      dbJsonResult.concat(",")
    end
  }
  return "[" + dbJsonResult + "]"
end

post ('/month') do
  print "MONTH"
  @monthlyBill = request.body.read
  @json = JSON.parse(@monthlyBill)

  # THIS QUERY BELOW WORKS FOR UPDATING JSON OBJECT IN POSTGRES, it replaces the entire column:
  # UPDATE fvckmoney.monthly_expenses SET monthly_expense = to_json('{"month":"august","bills":[{"name":"water","amount":330,"dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvc"},{"name":"trash","amount":334,"dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvc"},{"name":"gas","amount":314,"dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvc"},{"name":"pge","amount":398,"dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvc"},{"name":"internet","amount":368,"dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvc"}],"others":[{"note":"BRAND NEW EXPENSE","amount":"100.00","paidBy":"fvc"},{"amount":199999.99,"note":"Pan tostado que rico!!!","dateRegistered":"2014-01-01T00:00:00.000Z","paidBy":"fvckm"},{"amount":27.27,"note":"Me comi un caballo!!","dateRegistered":"2016-09-01T00:02:03.032Z","paidBy":"fvckm"}]}'::json) WHERE month = 'august'
  # OTHER FUNCTIONS AND ABILITY TO UPDATE PARTIAL JSON OBJECT IS AVAILABLE USING JSONB OBJECT.
  # FOR THIS APP MY JSON OBJECTS WILL ONLY BE 12 (one per month) and my bills are not many so it makes sense to store the entire monthly expense in one object.

  res = conn.exec("UPDATE fvckmoney.monthly_expenses SET monthly_expense = to_json('" + @monthlyBill + "'::json) WHERE month='" + @json['month'].to_s + "'")

  # @json now contains a hash of the submitted JSON content

  return "UPDATE success"
end

# post('/month', nil?, &body) do
#   content_type :json
#   request.body.rewind
#   request_payload = JSON.parse request.body.read
#
#   printf request_payload
# end


  # content_type :json
  # request.body.rewind
  # request_payload = JSON.parse request.body.read
  #
  # printf request_payload
  # "win"
  # content_type :json
  # conn = PG::Connection.open("localhost",5432,"","","postgres","administrator","password")
  # res = conn.exec("SELECT monthly_expense::json FROM fvckmoney.monthly_expenses")
  # i=0
  # dbJsonResult = ""
  # res.each{
  #   dbJsonResult.concat(res[i]['monthly_expense'].to_s)
  #   i=i+1
  #   if i< 9
  #     dbJsonResult.concat(",")
  #   end
  # }
  #
  # return "[" + dbJsonResult[0...-1] + "]"

# conn = PG::Connection.open("localhost",5432,"","","postgres","administrator","password")

# res = conn.exec("SELECT month, monthly_expense::json FROM fvckmoney.monthly_expenses")
# res.each{
#   printf(res[0]['month'] + " " + res[0]['monthly_expense'])
# }
#
#
# res = conn.exec("SELECT * FROM fvckmoney.users")
# res.each{
#   printf(res[0]['name'] + " " + res[0]['password'])
# }
#
#
# res = conn.exec("SELECT  FROM fvckmoney.users")
# res.each{
#   printf(res[0])
# }
#
#
# res=conn.exec("SELECT monthly_expense->'others'->>0 AS other FROM fvckmoney.monthly_expenses;")
# i=0
# res.each{
#  printf(res[i]['other'])
#  i+=1
# }
