package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

const (
	host     = "fvckm-db"
	port     = 5432
	user     = "administrator"
	password = "password"
	dbname   = "postgres"
)

var db *sql.DB

func main() {
	connectToDB()
	verifyDB()
	gorillaRoute := mux.NewRouter()
	gorillaRoute.HandleFunc("/months", getMonths).Methods("GET")
	gorillaRoute.HandleFunc("/month", postMonth).Methods("POST")

	// Apply the CORS middleware to our top-level router, with the defaults.
	http.Handle("/", &WithCORS{gorillaRoute})
	http.ListenAndServe(":8080", nil)
}

type WithCORS struct {
	r *mux.Router
}

// Simple wrapper to Allow CORS.
// See: http://stackoverflow.com/a/24818638/1058612.
func (s *WithCORS) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		res.Header().Set("Access-Control-Allow-Origin", origin)
		res.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		res.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}
	// Stop here for a Preflighted OPTIONS request.
	if req.Method == "OPTIONS" {
		return
	}
	// Lets Gorilla work
	s.r.ServeHTTP(res, req)
}

func getMonths(w http.ResponseWriter, r *http.Request) {
	rows, err := db.Query("SELECT id, monthly_expense::json FROM fvckmoney.monthly_expenses ORDER BY id")
	if err != nil {
		fmt.Println(" OH OOPS!")
		log.Fatal(err)
	}
	defer rows.Close()

	w.Write([]byte("["))
	for rows.Next() {
		var id int
		var whatever string
		if err := rows.Scan(&id, &whatever); err != nil {
			fmt.Println(" OH FANG!")
			log.Fatal(err)
		}
		fmt.Println(id)
		fmt.Println(string(whatever))

		w.Write([]byte(string(whatever)))
		if id < 12 {
			w.Write([]byte(","))
		}
	}
	w.Write([]byte("]"))
	if err := rows.Err(); err != nil {
		fmt.Println(" OH SNAP!")
		log.Fatal(err)
	}
}

func postMonth(w http.ResponseWriter, req *http.Request) {

	buf := bytes.NewBuffer(make([]byte, 0, req.ContentLength))
	_, err := buf.ReadFrom(req.Body)
	body := buf.Bytes()
	fmt.Println(string(body))
	if err != nil {
		fmt.Println("got error : ", err)
		panic(err)
	}

	decoder := json.NewDecoder(buf)
	var monthlyExpense struct {
		Month string
	}
	err = decoder.Decode(&monthlyExpense)
	if err != nil {
		panic(err)
	}
	defer req.Body.Close()
	fmt.Println(monthlyExpense.Month)

	var buffer bytes.Buffer
	buffer.WriteString("UPDATE fvckmoney.monthly_expenses SET monthly_expense = to_json('")
	buffer.WriteString(string(body))
	buffer.WriteString("'::json) WHERE month='" + monthlyExpense.Month + "'")

	res, err := db.Exec(buffer.String())
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	fmt.Println(res)
	w.Write([]byte("RESPONSE HERE!!!"))
}

func connectToDB() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	var err error
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("SUCESSfully connected!")
}

func verifyDB() {
	rows, err := db.Query("SELECT id FROM fvckmoney.monthly_expenses ORDER BY id")
	if err != nil {
		fmt.Println(" OH OOPS!", err)
		log.Fatal(err)
	}
	defer rows.Close()

	var id int
	for i := 1; i <= 12; i++ {
		rows.Next()
		if err := rows.Scan(&id); err != nil {
			fmt.Println(" OH FANG!")
			log.Fatal(err)
		}
		if i != id {
			fmt.Println(fmt.Sprintf("We are missing %v", i))
			month := time.Month(i).String()

			var buffer bytes.Buffer
			buffer.WriteString("INSERT INTO fvckmoney.monthly_expenses (id, month, monthly_expense) VALUES (")
			buffer.WriteString(fmt.Sprintf("%v, '"+month+"', to_json('{\"month\":\""+month+"\"}'::json))", i))

			fmt.Println("GOING TO USE: " + buffer.String())
			res, err := db.Exec(buffer.String())
			if err != nil {
				fmt.Println(err)
				panic(err)
			}

			fmt.Println(res)
			i++
		}
	}
}

func shutDown() {
	db.Close()
}
