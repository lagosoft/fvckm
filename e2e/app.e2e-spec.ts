import { FvckmMoneyPage } from './app.po';

describe('fvckm-money App', function() {
  let page: FvckmMoneyPage;

  beforeEach(() => {
    page = new FvckmMoneyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
