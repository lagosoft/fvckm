import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable}  from "rxjs/Observable";
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { IMonthlyBill } from '../../shared/monthlybill.interface';
import { Moment } from "moment";

@Component({
  selector: 'monthlist',
  templateUrl: './monthlist.component.html',
  styleUrls: ['./monthlist.component.css']
})
export class MonthlistComponent implements OnInit {

  @Input()
  monthlyBills: IMonthlyBill[];

  @Output()
  selectMonth = new EventEmitter<IMonthlyBill>();

  today:Date;
  currentMonth: IMonthlyBill;
  allMonths = [];

  constructor(private firebase: AngularFire) {
    this.today = new Date(Date.now());

    const months$: FirebaseListObservable<any> = firebase.database.list('months');
    months$.subscribe((firebasePayload) => {
      console.log("FIREBASE GOT VALUE: ", firebasePayload);
      this.setAllMonths(firebasePayload);
    });
  }

  setMonth(month){
    if(month.id > this.today.getMonth()+1) {
      // ignore...
    } else {
      this.currentMonth = month;
      this.selectMonth.emit(month);
    }
  }

  ngOnInit() {
  }

  setAllMonths(firebaseMonths){
    if(firebaseMonths.length === 0){
      months_names.forEach((name, index) => {
        const monIndex = index+1;
        this.firebase.database.object('months/' + monIndex)
          .set({
            month: name,
            id: monIndex
          });
      });
    }
    else{
      this.allMonths = firebaseMonths;
      this.setMonth(this.allMonths[this.today.getMonth()]);

    }
  }
}

const months_names = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
