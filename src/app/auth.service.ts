import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AngularFireAuth } from 'angularfire2/angularfire2';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuard implements CanActivate {

    currentUserName;
    loggedInUID;
    constructor(private auth: AngularFireAuth,
                private router: Router){}

    canActivate(): Observable<boolean> {
      return Observable.from(this.auth)
        .take(1)
        .map((state) => {
          console.log(state);
          this.currentUserName = state.auth.displayName;
          this.loggedInUID = state.auth.uid;
          return !!state;
        })
        .do(authenticated => {
            if (!authenticated) {
              this.router.navigate(['/login']);
            }
          }
        );
    }

    getCurrentUser(){
      console.log("RETURNING : ", this.currentUserName);
      return {username: this.currentUserName, uid: this.loggedInUID};
    }

  logout() {
    console.log("LOGGING OUT");
    this.auth.logout();
    this.router.navigate(['/login']);
    console.log("DONE");
  }
}
