import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods } from "angularfire2";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error: any;

  constructor(private firebase: AngularFire,
              private router: Router){

    this.firebase.auth.subscribe(auth => {
      if(auth) {
        this.router.navigateByUrl('/monthlyBills');
      }
    });
  }

  ngOnInit(){
  }

  loginFb() {
    this.firebase.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup
    }).then((success) => {
      this.router.navigate(['/monthlyBills']);
      }
    ).catch((error) => {
      this.error = error;
    });
  }

  loginGoogle() {
    this.firebase.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    }).then((auth) => {
      this.router.navigate(['/monthlyBills']);
    }).catch((error) => {
      this.error = error;
    });
  }
}
