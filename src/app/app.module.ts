import { NgModule, enableProdMode, CUSTOM_ELEMENTS_SCHEMA }       from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePickerModule } from 'ng2-datepicker'
import { AngularFireModule } from 'angularfire2';

import { AppComponent, environment }   from './';
import { MonthComponent } from './month'
import { MonthlistComponent } from './month/monthlist'
import { MonthlybillComponent } from './month/monthlybill';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component'
import { routes } from "app/app.routes";
import { AuthGuard } from "app/auth.service";

export const firebaseConfig = {
  apiKey: "AIzaSyCMhKSTmvNTkf9v_NmETSIcL85QsJZTDh4",
  authDomain: "fvckm-d6c01.firebaseapp.com",
  databaseURL: "https://fvckm-d6c01.firebaseio.com",
  storageBucket: "fvckm-d6c01.appspot.com",
  messagingSenderId: "443639469405"
};

if (environment.production) {
  enableProdMode();
}

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    DatePickerModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routes
  ],
  declarations: [
    AppComponent,
    MonthComponent,
    MonthlistComponent,
    MonthlybillComponent,
    UserComponent,
    LoginComponent,
  ],
  providers: [AuthGuard],
  schemas : [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap:    [AppComponent]
})
export class AppModule {}
