import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { IMonthlyBill } from './../shared/monthlybill.interface';
import { AuthGuard } from './../auth.service';

@Component({
  selector: 'month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.css']
})
export class MonthComponent implements OnInit {
  currentUser;
  selectedMonth$;
  today:Date;
  allMonths : FirebaseListObservable<IMonthlyBill[]>;
  allowedUsers : FirebaseListObservable<any>;
  private authorized:boolean;

  constructor(private firebase: AngularFire,
              private router: Router,
              private authSvc: AuthGuard) {

    this.today = new Date(Date.now());
    this.allMonths = firebase.database.list('months');
    this.allowedUsers = firebase.database.list('users');
  }

  ngOnInit() {

    if( !this.currentUser ) {
       this.currentUser = this.authSvc.getCurrentUser();
    }

    this.allowedUsers.subscribe((users) => {
      console.log("allowed USERS = ", users[0].$key);
      users.forEach((user) => {
        if(user.$key === this.currentUser.uid){
          console.log("Granted!");
          this.authorized = true;
          this.currentUser = this.authSvc.getCurrentUser();
        }
      });

      if(false === this.authorized){
        this.currentUser = null;
        this.authorized = false;
      }
    });
  }

  receiveSelectedMonth(month) {
    try{
      this.selectedMonth$ = month;
    }catch(e) {
      console.error("ERROR receiving selected : ");
    }
  }

  updateMonthWithChanges() {
    let changes = <IMonthlyBill>{};
    if ( this.selectedMonth$.bills ) {
      changes.bills  = this.selectedMonth$.bills
    };
    if ( this.selectedMonth$.others ) {
      changes.others  = this.selectedMonth$.others
    };
    this.allMonths.update(this.selectedMonth$.$key, changes);
  }
}
